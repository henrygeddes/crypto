const crypto 	= require('crypto');
const request 	= require('request');

const API_KEY 		= 'Oh5PGCdIfz5zxHs0RIRD2Hnv1HADKdfyVDloaUFId8H';
const API_SECRET 	= 'CdWg753pL8QfOUR823uun39DQ0BSkTdGvGBSTg1irIX';


const path 	= 'v2/auth/r/trades/hist';
const body = { };

const rawBody = JSON.stringify(body);

const nonce = Date.now().toString();
let signature = `/api/${path}${nonce}${rawBody}`;

signature = (
	crypto
		.createHmac('sha384', API_SECRET)
		.update(signature)
		.digest('hex')
);

const options = {
 	url: `https://api.bitfinex.com/${path}`,
 	headers: {
		'bfx-nonce': nonce,
		'bfx-apikey': API_KEY,
		'bfx-signature': signature
	},
	body: body,
	json: true
};

request.post(options, (error, response, body) => {
	if( error )
		throw error;

	//body = JSON.parse(body);
	const orders = body.map(order => {
		// For endpoint ORDERS

		return {
			ids: {
				trade: order[0],
				order: order[3]
			},
			pair: order[1],
			type: order[4] > 0 ? 'buy' : 'sell',
			price: order[5],
			amount: Math.abs(order[4]),
			orderType: order[6],
			created: order[2],
			askPrice: order[7],
			maker: order[8],
			fee: {
				amount: Math.abs(order[9]),
				currency: order[10]
			}
		};
	
		/*
		return {
			id: order[0],
			created: order[4],
			updated: order[5],
			pair: order[3],
			price: order[17],
			placedPrice: order[16],
			amount: order[7],
			type: order[6] > 0 ? 'buy' : 'sell',
			orderType: order[8],
			status: order[13],
		};
		*/
	});

	console.log(orders);
});
