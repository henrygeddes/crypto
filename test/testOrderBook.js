const assert = require('assert');
const OrderBook = require('../apis/orderBook.js');

const orderBook = new OrderBook();

describe('orderBook', () => {
	// more chunk tests here

	describe('_make', () => {
		const orders = [
			{ price: 1.15, amount: 1, type: 'buy' },
			{ price: 1.1, amount: 0.7, type: 'sell' },
		];

		orders.forEach(o => orderBook.make(o));

		it('adds trade correctly', () => {
			assert.equal(orderBook.trades.length, 1);
		});

		it('calculates trade', () => {
			const trade = orderBook.trades[orderBook.trades.length - 1];

			assert.equal(trade.price, 1.15);
			assert.equal(trade.amount, 0.7);
		});
	});

});
