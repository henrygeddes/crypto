const assert = require('assert');
const Helper = require('../app');

describe('chunk', () => {
	// more chunk tests here

	describe('_reduceChunk', () => {

		it('matches non chunked results', () => {
			const trades = require('./mockTrades');
			const helper = new Helper(trades);

			const chunk = helper.chunk();
			const reducedChunk = helper.reduceChunk(chunk);

			const reducedTrades = trades.reduce((res, trade) => {
				res.amount += Math.abs(trade.amount);
				res.total += Math.abs(trade.price);
				res.trades++;

				return res;
			}, helper._initialChunk());

			assert.equal(reducedChunk.amount, reducedTrades.amount);
			assert.equal(reducedChunk.total, reducedTrades.total);
			assert.equal(reducedChunk.trades, reducedTrades.trades);
		});

		it('calculates correctly', () => {
			const trades = [
				{ "price": 1, "amount": 1, "timestamp":1505540816 },
				{ "price": 2, "amount": 1, "timestamp":1505540816 },
				{ "price": 3, "amount": 1, "timestamp":1505540816 },
				{ "price": 4, "amount": 1, "timestamp":1505540816 },
				{ "price": 5, "amount": 1, "timestamp":1505540816 }
			];

			const helper = new Helper(trades);
			const chunk = helper.chunk();
			const reduced = helper.reduceChunk(chunk);

			assert.equal(reduced.averagePrice, 3);
			assert.equal(reduced.amount, 5);
			assert.equal(reduced.total, 15);
			assert.equal(reduced.trades, 5);
			assert.equal(reduced.low, 1);
			assert.equal(reduced.high, 5);
		});

		// more _reduceChunk tests here
	});

});