const OrderBook = require('./orderBook');

/*
const orders = [
	{ type: 'buy', price: 0, amount: 1 },
	{ type: 'buy', price: 0, amount: 1 },
	{ type: 'buy', price: 0, amount: 1 },
	{ type: 'buy', price: 0, amount: 1 },
	{ type: 'buy', price: 0, amount: 1 },
	{ type: 'buy', price: 0, amount: 1 },
	{ type: 'sell', price: 0, amount: 1 },
	{ type: 'sell', price: 0, amount: 1 },
	{ type: 'sell', price: 0, amount: 1 },
	{ type: 'buy', price: 786.75, amount: 39.17645357 },
	{ type: 'buy', price: 785.79, amount: 28.82311614 },
	{ type: 'buy', price: 785.78, amount: 15 },
	{ type: 'buy', price: 785.6, amount: 3.96 },
	{ type: 'buy', price: 785.06, amount: 2.39882833 },
	{ type: 'buy', price: 784.75, amount: 2.39621777 },
	{ type: 'sell', price: 786.89, amount: 3.22354643 },
	{ type: 'sell', price: 787.57, amount: 0.34315345 },
	{ type: 'sell', price: 787.88, amount: 0.15383481 },
	{ type: 'buy', price: 0, amount: 1 },
	{ type: 'buy', price: 0, amount: 1 },
	{ type: 'buy', price: 0, amount: 1 },
	{ type: 'buy', price: 0, amount: 1 },
	{ type: 'buy', price: 0, amount: 1 },
	{ type: 'buy', price: 0, amount: 1 },
	{ type: 'sell', price: 0, amount: 1 },
	{ type: 'sell', price: 0, amount: 1 },
	{ type: 'sell', price: 0, amount: 1 },
	{ type: 'sell', price: 0, amount: 1 },
	{ type: 'buy', price: 786.76, amount: 90.3 },
	{ type: 'buy', price: 786.75, amount: 6.74176204 },
	{ type: 'buy', price: 786.51, amount: 20.7345 },
	{ type: 'buy', price: 785.56, amount: 28.82311614 },
	{ type: 'buy', price: 784.92, amount: 4.79328761 },
	{ type: 'buy', price: 784.7, amount: 32.4705 },
	{ type: 'sell', price: 787, amount: 0.14192776 },
	{ type: 'sell', price: 787.88, amount: 5.16591296 },
	{ type: 'sell', price: 788.66, amount: 15.6256 },
	{ type: 'sell', price: 788.72, amount: 0.05 },
	{ type: 'sell', price: 788.8, amount: 25 },
	{ type: 'buy', price: 0, amount: 1 },
	{ type: 'buy', price: 0, amount: 1 },
	{ type: 'buy', price: 0, amount: 1 },
	{ type: 'buy', price: 0, amount: 1 },
	{ type: 'buy', price: 0, amount: 1 },
	{ type: 'buy', price: 0, amount: 1 },
	{ type: 'buy', price: 0, amount: 1 },
	{ type: 'buy', price: 0, amount: 1 },
	{ type: 'sell', price: 0, amount: 1 },
	{ type: 'sell', price: 0, amount: 1 },
	{ type: 'buy', price: 786.77, amount: 90.3 },
	{ type: 'buy', price: 786.76, amount: 1.8 },
	{ type: 'buy', price: 786.76, amount: 7.18275773 },
	{ type: 'buy', price: 785.8, amount: 2.4804 },
	{ type: 'buy', price: 785.8, amount: 28.82311614 },
	{ type: 'buy', price: 785.79, amount: 1.0196 },
	{ type: 'buy', price: 785.01, amount: 3.38895455 },
	{ type: 'buy', price: 784.61, amount: 4.79783608 },
	{ type: 'sell', price: 788.79, amount: 0.05 },
	{ type: 'sell', price: 788.97, amount: 1.80006194 },
	{ type: 'buy', price: 0, amount: 1 },
	{ type: 'buy', price: 0, amount: 1 },
	{ type: 'buy', price: 0, amount: 1 },
	{ type: 'sell', price: 0, amount: 1 },
	{ type: 'sell', price: 0, amount: 1 },
	{ type: 'sell', price: 0, amount: 1 },
	{ type: 'buy', price: 786.78, amount: 1.8 },
	{ type: 'buy', price: 786.77, amount: 42.4 },
	{ type: 'buy', price: 786.51, amount: 15 },
	{ type: 'sell', price: 787, amount: 0.04192776 },
	{ type: 'sell', price: 788.51, amount: 5.16591296 },
	{ type: 'sell', price: 788.68, amount: 15.6391 },
	{ type: 'sell', price: 788.85, amount: 0.05 },
	{ type: 'buy', price: 0, amount: 1 },
	{ type: 'buy', price: 0, amount: 1 },
	{ type: 'buy', price: 0, amount: 1 },
	{ type: 'buy', price: 0, amount: 1 },
	{ type: 'buy', price: 0, amount: 1 },
	{ type: 'sell', price: 0, amount: 1 },
	{ type: 'sell', price: 0, amount: 1 },
	{ type: 'sell', price: 0, amount: 1 },
	{ type: 'buy', price: 786.79, amount: 90.5 },
	{ type: 'buy', price: 786.79, amount: 19.3432 },
	{ type: 'buy', price: 786.78, amount: 3.6 },
	{ type: 'buy', price: 785.8, amount: 15 },
	{ type: 'buy', price: 784.7, amount: 4.79113154 },
	{ type: 'sell', price: 787.6, amount: 0.34315345 },
	{ type: 'sell', price: 788.86, amount: 0.05 },
	{ type: 'sell', price: 788.94, amount: 4.7931294 },
	{ type: 'buy', price: 0, amount: 1 },
	{ type: 'buy', price: 0, amount: 1 },
	{ type: 'buy', price: 0, amount: 1 },
	{ type: 'buy', price: 0, amount: 1 },
	{ type: 'buy', price: 0, amount: 1 },
	{ type: 'buy', price: 0, amount: 1 },
	{ type: 'sell', price: 0, amount: 1 },
	{ type: 'sell', price: 0, amount: 1 },
	{ type: 'sell', price: 0, amount: 1 },
	{ type: 'buy', price: 786.8, amount: 1.8 },
	{ type: 'buy', price: 786.45, amount: 3.96 },
	{ type: 'buy', price: 786.4, amount: 2.2743 },
	{ type: 'buy', price: 785.54, amount: 1 },
	{ type: 'buy', price: 785.36, amount: 2.39734326 },
	{ type: 'buy', price: 785.29, amount: 4.79218837 },
	{ type: 'sell', price: 787.59, amount: 0.34315345 },
	{ type: 'sell', price: 788.2, amount: 0.3657 },
	{ type: 'sell', price: 788.87, amount: 0.05 },
	{ type: 'buy', price: 0, amount: 1 },
	{ type: 'buy', price: 0, amount: 1 },
	{ type: 'buy', price: 786.79, amount: 15 },
	{ type: 'buy', price: 784.44, amount: 1.25788612 },
	{ type: 'buy', price: 0, amount: 1 },
	{ type: 'buy', price: 0, amount: 1 },
	{ type: 'buy', price: 0, amount: 1 },
	{ type: 'buy', price: 0, amount: 1 },
	{ type: 'buy', price: 0, amount: 1 },
	{ type: 'buy', price: 0, amount: 1 },
	{ type: 'buy', price: 0, amount: 1 },
	{ type: 'sell', price: 0, amount: 1 },
	{ type: 'sell', price: 0, amount: 1 },
	{ type: 'sell', price: 0, amount: 1 },
	{ type: 'buy', price: 786.81, amount: 90.7 },
	{ type: 'buy', price: 786.8, amount: 3.6 },
	{ type: 'buy', price: 785.8, amount: 15 },
	{ type: 'buy', price: 785.67, amount: 4.79246515 },
	{ type: 'buy', price: 785.63, amount: 2.39769677 },
	{ type: 'buy', price: 785.22, amount: 4.79507002 },
	{ type: 'buy', price: 785.22, amount: 2.39568332 },
	{ type: 'sell', price: 787.87, amount: 0.34315345 },
	{ type: 'sell', price: 788.24, amount: 5.16591296 },
	{ type: 'sell', price: 788.48, amount: 0.3657 },
	{ type: 'buy', price: 0, amount: 1 },
	{ type: 'buy', price: 0, amount: 1 },
	{ type: 'buy', price: 0, amount: 1 },
	{ type: 'sell', price: 0, amount: 1 },
	{ type: 'sell', price: 0, amount: 1 },
	{ type: 'buy', price: 786.82, amount: 1.8 },
	{ type: 'buy', price: 786.81, amount: 7.30712302 },
	{ type: 'buy', price: 786.4, amount: 15 },
	{ type: 'sell', price: 788.97, amount: 1.80006194 },
	{ type: 'sell', price: 789, amount: 0.05006155 },
	{ type: 'buy', price: 0, amount: 1 },
	{ type: 'buy', price: 0, amount: 1 },
	{ type: 'buy', price: 0, amount: 1 },
	{ type: 'buy', price: 0, amount: 1 },
	{ type: 'buy', price: 0, amount: 1 },
	{ type: 'buy', price: 0, amount: 1 },
	{ type: 'sell', price: 0, amount: 1 },
	{ type: 'buy', price: 786.82, amount: 3.6 },
	{ type: 'buy', price: 786.81, amount: 19.3465 },
	{ type: 'buy', price: 785.81, amount: 2.0943 },
	{ type: 'buy', price: 784.44, amount: 1.25788612 },
	{ type: 'buy', price: 784.28, amount: 30 },
	{ type: 'buy', price: 784.27, amount: 23.96537828 },
	{ type: 'sell', price: 788.88, amount: 0.05 },
	{ type: 'buy', price: 0, amount: 1 },
	{ type: 'buy', price: 0, amount: 1 },
	{ type: 'buy', price: 0, amount: 1 },
	{ type: 'buy', price: 0, amount: 1 },
	{ type: 'buy', price: 0, amount: 1 },
	{ type: 'buy', price: 0, amount: 1 },
	{ type: 'sell', price: 0, amount: 1 },
	{ type: 'buy', price: 786.83, amount: 90.7 },
	{ type: 'buy', price: 786.74, amount: 3 },
	{ type: 'buy', price: 785.82, amount: 28.82311614 },
	{ type: 'buy', price: 785.61, amount: 2.39611918 },
	{ type: 'buy', price: 785.4, amount: 11.98051584 },
	{ type: 'buy', price: 785.28, amount: 4.79226448 },
	{ type: 'sell', price: 787.89, amount: 0.34315345 },
	{ type: 'buy', price: 0, amount: 1 },
	{ type: 'buy', price: 0, amount: 1 },
	{ type: 'buy', price: 0, amount: 1 },
	{ type: 'buy', price: 0, amount: 1 },
	{ type: 'sell', price: 0, amount: 1 },
	{ type: 'sell', price: 0, amount: 1 },
	{ type: 'buy', price: 786.84, amount: 90.7 },
	{ type: 'buy', price: 786.84, amount: 1.8 },
	{ type: 'buy', price: 786.84, amount: 3.6 },
	{ type: 'buy', price: 786.46, amount: 3.96 },
	{ type: 'sell', price: 788.89, amount: 0.05 },
	{ type: 'sell', price: 789, amount: 0.05006155 },
	{ type: 'buy', price: 0, amount: 1 },
	{ type: 'buy', price: 0, amount: 1 },
	{ type: 'buy', price: 0, amount: 1 },
	{ type: 'sell', price: 0, amount: 1 },
	{ type: 'sell', price: 0, amount: 1 },
	{ type: 'buy', price: 786.85, amount: 90.9 },
	{ type: 'buy', price: 786.83, amount: 7.30712302 },
	{ type: 'buy', price: 785.84, amount: 2.39715655 },
	{ type: 'sell', price: 788.5, amount: 0.15383481 },
	{ type: 'sell', price: 788.88, amount: 0.05 },
];
*/

const orders = [
	{ type: 'buy', price: 700, amount: 1 },
	{ type: 'sell', price: 700, amount: 1.5 },
	{ type: 'buy', price: 705, amount: 0.7 }
];

// - - - - - - - - - - - - - - - - - - - - - - -

// Main
const orderBook = new OrderBook();

orders.forEach(order => {
	if( order.price === 0 )
		orderBook.cancel(order);
	else
		orderBook.make(order);
});

console.log('TRADES: ');
console.log(orderBook.trades);
console.log('');
console.log('ORDERS: ');
console.log(orderBook.orders);
