const request = require('superagent');
const WebSocket = require('ws');

// NOTES
// not sure if auth works properly, it fails 80% of the time (but connects eventually)
// due to 400 (bad request)

// auth
request
.get('https://www.cryptopia.co.nz/signalr/negotiate?clientProtocol=1.5')
.end((err, res) => {
	if( err )
		throw err;

	const token = res.body.ConnectionToken;


	const url = `wss://www.cryptopia.co.nz/signalr/connect?transport=webSockets&clientProtocol=1.5&connectionToken=${token}&connectionData=%5B%7B%22name%22%3A%22notificationhub%22%7D%5D&tid=10`;

	const ws = new WebSocket(url, {
		// options
	});

	ws.on('open', () => {
		console.log('Connection opened');	
	});

	try{
		ws.on('message', data => {
			console.log(data);
			console.log('- - - - - - - - - - - -');
		});
	} catch(e) {
		throw e;
	}	
});

