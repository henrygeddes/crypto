// Dependencies
const BigNumber = require('bignumber.js');
const ws = require('ws');
const w = new ws('wss://api.bitfinex.com/ws/2');

const OrderBook = require('./orderBook');
// - - - - - - - - - - - - - - - - - - - - - - -

// Config
const PAIR = 'ETHUSD';

// Main
const orderBook = new OrderBook();
const orderMap = {};
const rawOrders = [];

var tradeCount = 0;

// For logging
process.on('SIGINT', () => { 
	console.log('\ntrades'); 
	console.log(orderBook.trades); 
	orderBook.logOrders();

	// console.log('Raw Orders');
	// console.log(rawOrders);

	process.exit(); 
});

let isFirst = true;
// Handle stream
w.on('message', msgJson => {
	const msg = JSON.parse(msgJson);

	// Deal with non-data msg's
	if( ! Array.isArray(msg) ) {
		console.log(msg);
		return;
	} 

	// @TODO figure out what this is
	// Skip first order stream (no idea what it is)
	if( isFirst ) {
		isFirst = false;
		return;
	}

	const streamId 	= msg[0];
	const data 		= msg[1];

	// Ignore heartbeat
	if( data === 'hb' )
		return;

	const orderId 	= data[0];
	const price 	= data[1];
	const amount	= data[2];

	const isBuy = amount > 0;

	var order = { price: price, amount: Math.abs(amount), type: isBuy ? 'buy' : 'sell' };
	rawOrders.push(Object.assign({}, order));

	if( price === 0 ) {
		if( orderMap[orderId] ) {
			order = Object.assign({}, orderMap[orderId]);
			orderBook.cancel(order);
		} else {
			//console.log(`Cant find ${orderId} to cancel, amount: ${amount}`);
		}
	} else {
		orderBook.make(order);
		orderMap[orderId] = { price: price, amount: Math.abs(amount) };
	}



	if( orderBook.trades.length > tradeCount ) {
		console.log(`Last: ${orderBook.trades[orderBook.trades.length - 1].price}`);
		tradeCount++;
	}

	const buyTop = orderBook.get(orderBook.orders.buys, true);
	const sellBot = orderBook.get(orderBook.orders.sells, false);
	//console.log(`${buyTop ? buyTop.price : 'N'} : ${sellBot ? sellBot.price : 'N'}`);
});

// Setup WS for BTCUSD
let msg = { 
	event: 'subscribe', 
	channel: 'book', 
	symbol: PAIR,
	prec: 'R0',
	freq: 'F0',
	len: 25
};
w.on('open', () => w.send(JSON.stringify(msg)) );

