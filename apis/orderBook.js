const BigNumber = require('bignumber.js');

class OrderBook {
	constructor() {
		this.orders = { buys: {}, sells: {} };
		this.trades = [];
	}

	sort(orders, isHighest) {
		if( ! Object.keys(orders).length )
			return [];

		return Object.keys(orders).sort((a, b) => {
			a = typeof a === 'string' ? new BigNumber( Math.abs(a) ) : a;
			b = typeof b === 'string' ? new BigNumber( Math.abs(b) ) : b;

			return isHighest ? b.minus(a) : a.minus(b);
		});
	}

	logOrders() {
		const output = (orders, isHighest) => {
			const ordered = this.sort(orders, isHighest);

			if( ! ordered.length ) {
				console.log('None');
			}

			ordered.forEach( o => {
				console.log(`P: ${o}, A: ${orders[o].toString()}`);
			});
		};

		console.log('- - - - - - - - - - -');
		console.log('Buys');
		output( this.orders.buys, true );
		console.log('- - - - - - - - - - -');
		console.log('Sells');
		output( this.orders.sells, false );
		console.log('- - - - - - - - - - -');
	}

	get(orders, isHighest) {
		if( ! Object.keys(orders).length )
			return false;

		const ordered = this.sort(orders, isHighest);

		return { price: new BigNumber(ordered[0]), amount: orders[ordered[0]] }; 
	}

	make(order) {
		if( typeof order.amount === 'number' )
			order.amount = new BigNumber(order.amount);

		if( typeof order.price === 'number' )
			order.price  = new BigNumber(order.price);

		if( order.type === 'buy' ) {
			const sellBottom = this.get(this.orders.sells, false);
			const sellBottomPrice = sellBottom ? sellBottom.price.toString() : false;
			
			// insta fill
			if( sellBottom !== false && sellBottom.price.lessThanOrEqualTo(order.price) ) {
				if( sellBottom.amount.greaterThanOrEqualTo( order.amount ) ) {
					// Buy order fully filled
					this.orders.sells[sellBottomPrice] = this.orders.sells[sellBottomPrice].minus(order.amount);

					this.trades.push({
						price: sellBottomPrice,
						amount: order.amount.toString(),
						filled: this.orders.sells[sellBottomPrice].isZero() ? 'full' : 'partial',
						type: order.type
					});

					if( this.orders.sells[sellBottomPrice].isZero() )
						delete this.orders.sells[sellBottomPrice];
				} else {
					// Buy order needs to be split
					delete this.orders.sells[sellBottomPrice];
					order.amount = order.amount.minus(sellBottom.amount);

					this.trades.push({
						price: sellBottomPrice,
						amount: sellBottom.amount.toString(),
						filled: 'partial',
						type: order.type
					});

					if( order.amount.lessThan(0) ) {
						console.log('ERROR: ', order);
						console.log('SellBottom: ', sellBottom);
						console.log('');
					}

					this.make(order); 
				}
			} else {
				// Add order to book
				const orderPrice = order.price.toString();

				// add order
				if( this.orders.buys[orderPrice] ) {
					this.orders.buys[orderPrice] = this.orders.buys[orderPrice].plus(order.amount);
				} else {
					this.orders.buys[orderPrice] = order.amount;
				}
			}
		} else {
			const buyTop 	 = this.get(this.orders.buys, true);
			const buyTopPrice = buyTop ? buyTop.price.toString() : false;
			
			// insta fill
			if( buyTop !== false && buyTop.price.greaterThanOrEqualTo(order.price) ) {
				if( buyTop.amount.greaterThanOrEqualTo(order.amount) ) {
					// Sell order fully filled
					this.orders.buys[buyTopPrice] = this.orders.buys[buyTopPrice].minus(order.amount);

					this.trades.push({
						price: buyTopPrice,
						amount: order.amount.toString(),
						filled: this.orders.buys[buyTopPrice].isZero() ? 'full' : 'partial',
						type: order.type
					});

					if( this.orders.buys[buyTopPrice].isZero() )
						delete this.orders.buys[buyTopPrice];

				} else {
					// Sell order replaced
					delete this.orders.buys[buyTopPrice];
					order.amount = order.amount.minus(buyTop.amount);

					if( order.amount.lessThan(0) ) {
						console.log('ERROR: ', order);
						console.log('BuyTop: ', buyTop);
						console.log('');
					}

					this.trades.push({
						price: buyTopPrice,
						amount: buyTop.amount.toString(),
						filled: 'partial',
						type: order.type
					});

					this.make(order); 
				}
			} else {
				// add order
				const orderPrice = order.price.toString();

				if( this.orders.sells[orderPrice] ) {
					this.orders.sells[orderPrice] = this.orders.sells[orderPrice].plus(order.amount);
				} else {
					this.orders.sells[orderPrice] = order.amount;
				}
			}
		}
	}

	cancel(order) {
		if( typeof order.amount === 'number' )
			order.amount = new BigNumber(order.amount);

		if( typeof order.price === 'number' )
			order.price  = new BigNumber(order.price);

		const orderPrice = order.price.toString();

		if( order.type === 'buy' ) {
			if( this.orders.buys[orderPrice] ) {
				this.orders.buys[orderPrice] = this.orders.buys[orderPrice].minus(order.amount);

				if( this.orders.buys[orderPrice].isZero() )
					delete this.orders.buys[orderPrice];
			}

		} else {
			if( this.orders.sells[orderPrice] ) {
				this.orders.sells[orderPrice] = this.orders.sells[orderPrice].minus(order.amount);

				if( this.orders.sells[orderPrice].isZero() )
					delete this.orders.sells[orderPrice];
			}
		}
	}
}

module.exports = OrderBook;
