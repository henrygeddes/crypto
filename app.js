// - - - - - - - - - - - - - - -
// @Dependancies
const fs = require('fs');
const moment = require('moment');



// - - - - - - - - - - - - - - -
// @Constants

// Debug
const DEBUG = true;
const LOGGING = 0; // 0 = none, 1 = important, 2 = all

// Internal
const TIMESTAMP_LENGTH = 10; // taken from trades[0].timestamp.length

// Not used
const MAX_INT = Number.MAX_SAFE_INTEGER;
const MIN_INT = 0;



// @TODO
// chunks should have their own class
// this way we can add methods to category or meta chunks (individually)
//
// MovingAverages:
// main function is done (but not sure if it actually works, need to check over)
// problem is we don't have averagePrice in the chunks, as we only add them in reduced chunks
// so we need to add averagePrice WITHOUT reducing the chunk
//
// or only add this to reduced chunks?

/* Notes: 
	
	Trade object:
		{"_id":{"$oid":"59bcbae0e11f5335bb200b8a"},"seq":67398318,"timestamp":1505540816,"price":0.505,"amount":836.312711}

	Chunks:
		- a chunk is ALWAYS an object
		- chunks can be either category chunks or meta chunks
			- category chunks can contain many other category OR meta chunks (but only one type)
*/



// - - - - - - - - - - - - - - -
// @Main

class Helper {
	constructor(trades) {
		this.activeTrades = trades;
		this.activeFilters = {}; // @TODO implement
		
		this._trades = trades;
		this._log = [];

		const metaChunk = (metaType = 'meta') => ({
			amount: 0, 
			trades: 0, 
			total: 0,
			low: null,
			high: null,
			//_type: metaType
			// averagePrice: 0 (this is only on reducedChunks)
		});

		this._initialChunk = (type => { 
			switch(type) {
				case 'category':
					return { }; // @NOTE YOU CANNOT HAVE _type HERE, IT WILL CAUSE CALL STACK TO BLOWUP
				default:
					return metaChunk(type);
			}
		});

		// this._initialChunk = () => ({
		// 		amount: 0, 
		// 		trades: 0, 
		// 		total: 0,
		// 		low: null,
		// 		high: null,
		// 		_type: 'meta',
		// 		// averagePrice: 0 (this is only on reducedChunks)
		// });

		if( LOGGING )
			this._log.push(`helper.constructor(${trades.length} trades)`);
	}

	// ** Actions **

	movingAverages(trades = null) {
		if( LOGGING )
			this._log.push(`helper.movingAverages(${trades ? trades.length + ' trades' : ''})`);

		if( ! trades )
			trades = this.activeTrades;

		let chunked = this.chunk(null, false, trades);

		//@NOTE FOR TESTING ONLY
		chunked = chunked._2017._9;

		// map chunk here, figure out averages
		let chunkArr = Object.keys(chunked).map(key => chunked[key] );

		const getMovingAverage = (chunk, i) => {
			if( chunk.amount ) {
				const prevChunk = i ? chunkArr[i - 1] : false;
				chunk.movingAverage = prevChunk ? chunk.averagePrice / prevChunk.averagePrice : 0;

				return chunk;
			} else {
				return Object.keys(chunk).map(key => getMovingAverage(chunk[key]));
			}
		};

		chunkArr = chunkArr.map((chunk, i) => getMovingAverage(chunk, i));

		console.log((chunkArr[0][0][0]));
		return;
		/*
		chunkArr = chunkArr.forEach(( chunk, i ) => {
			if( chunk.amount ) {
				return getMovingAverage(chunk, i);
			} else {
				// category chunk, need to do the same for each chunk
			}
		});
		*/

		console.log((chunkArr[0]['_17']['_46']));

		/* plan: 
			chunk down to minute level
			
		*/

		
		//const averages = [];
		// keys.forEach((key, i) => {
		// 	const prev = i ? keys[i - 1] : null;
		// 	const curr = chunked[key];

		// 	if( ! prev )
		// 		return 0;			
		// });

		//console.log(averages);
	}

	recursiveChunkAction(chunk, fnAction) {
		// if its a meta chunk
		if( chunk.amount )
			return fnAction(chunk);

		// Otherwise we know it's a category chunk
		let chunkArr = Object.keys(chunk).map(key => chunk[key] );

		// shouldn't happen, but if something goes horribly wrong we want a safe default
		if( ! chunkArr.length ){
			//return this._initialChunk();
			return 'something'; // @TODO how do we handle this?
		}

		// if it's a chunk category
		if( typeof chunkArr[0].amount === "undefined" )
			chunkArr = chunkArr.map( o => this.recursiveChunkAction(o, fnAction) ); //.reduce((res, o) => res.concat(o), []);

		// otherwise reduce the chunk
		// return chunkArr.reduce((res, chunk) => {

		// 	res.amount += Math.abs(chunk.amount);
		// 	res.trades += chunk.trades;
		// 	res.total  += Math.abs(chunk.total);

		// 	res.high    = res.high === null ? chunk.high : ( chunk.high > res.high ? chunk.high : res.high );
		// 	res.low     = res.low  === null ? chunk.low  : ( chunk.low  < res.low  ? chunk.low  : res.low  );

		// 	res.averagePrice = res.total / res.trades;

		// 	return res;
		// }, this._initialChunk());
	}

	// @TODO add intervals, only supports [hour][minute] atm
	chunk(intervalFormat = null, shouldReduce = false, trades = null) {
		if( LOGGING )
			this._log.push(`helper.chunk(${intervalFormat}, ${shouldReduce}, ${trades ? trades.length + ' trades' : ''})`);

		if( ! trades )
			trades = this.activeTrades;

		// seperate into 
		// [year][month][day][hour][minute]
		const chunked = (
			trades.reduce(( res, trade ) => {
				const date 	 = this._timestampToDate(trade.timestamp, "YYYY/M/D").split('/')
				
				// Add '_' so object key is a string
				const year 	 = '_' + date[0];
				const month	 = '_' + date[1];
				const day 	 = '_' + date[2];

				const time 	 = this._timestampToDate(trade.timestamp, "H:m").split(':');

				const hour 	 = '_' + time[0];
				const minute = '_' + time[1];

				// Setup default array if needed, must be a better way to do this...
				if( typeof res[year] === "undefined" )
					res[year] = this._initialChunk('category');
				if( typeof res[year][month] === "undefined" )
					res[year][month] = this._initialChunk('category');
				if( typeof res[year][month][day] === "undefined" )
					res[year][month][day] = this._initialChunk('category');
				if( typeof res[year][month][day][hour] === "undefined" )
					res[year][month][day][hour] = this._initialChunk('category');
				if( typeof res[year][month][day][hour][minute] === "undefined" )
					res[year][month][day][hour][minute] = this._initialChunk('meta');

				// get activeChunk
				const chunk = res[year][month][day][hour][minute];

				// @TODO should deal with 'negative' amounts somehow
				// need to figure out what they actually mean, but apparently its
				// price shift (positive or negative)

				// currently just treating negatives as positives

				// update chunk values
				chunk.amount += Math.abs(trade.amount);
				chunk.total += Math.abs(trade.price);

				chunk.trades++;

				chunk.high    = chunk.high === null ? trade.price : ( trade.price > chunk.high ? trade.price : chunk.high );
				chunk.low     = chunk.low  === null ? trade.price : ( trade.price < chunk.low  ? trade.price : chunk.low  );

				
				// update the chunk	
				res[year][month][day][hour][minute] = chunk;

				return res;
			}, {})
		);

		/* example chunked output
			{
				_2017: {
					_9: {
						_17: {
							0 : { ...initialChunk },
							1 : { ...initialChunk },
							2 : { ...initialChunk },
							...   ,
							23: { ...initialChunk }
						}
					}
				}
			}
		*/

		// reduce into
		// chunked._2017._9._17

		if( shouldReduce )
			var reduced = this.reduceChunk(chunked);


		if( DEBUG ){
			if( typeof chunked !== 'undefined' )
				fs.writeFileSync('debug/chunked.json', JSON.stringify(chunked), 'utf8');

			if( typeof reduced !== 'undefined' )
				fs.writeFileSync('debug/reduced.json', JSON.stringify(reduced), 'utf8');
		}

		return typeof reduced !== 'undefined' ? reduced : chunked;
	}
	reduceChunk(chunk) {
		if( LOGGING )
			this._log.push(`helper.reduceChunk({ ${Object.keys(chunk).slice(0, 5).join(', ')}, ... (${Object.keys(chunk).length} keys) })`);

		// Don't reduce if its a meta chunk (one with trade data on it)
		if( chunk.amount )
			return chunk;

		// Otherwise we know it's a category chunk
		let chunkArr = Object.keys(chunk).map(key => chunk[key] );

		// shouldn't happen, but if something goes horribly wrong we want a safe default chunk (mostly for the map reduce)
		if( ! chunkArr.length )
			return this._initialChunk('meta');

		// if it's a chunk category
		if( typeof chunkArr[0].amount === "undefined" )
			chunkArr = chunkArr.map(o => this.reduceChunk(o)).reduce((res, o) => res.concat(o), []);

		// otherwise reduce the chunk
		return chunkArr.reduce((res, chunk) => {

			res.amount += Math.abs(chunk.amount);
			res.trades += chunk.trades;
			res.total  += Math.abs(chunk.total);

			res.high    = res.high === null ? chunk.high : ( chunk.high > res.high ? chunk.high : res.high );
			res.low     = res.low  === null ? chunk.low  : ( chunk.low  < res.low  ? chunk.low  : res.low  );

			res.averagePrice = res.total / res.trades;

			return res;
		}, this._initialChunk('metaReduced'));
	}

	// @TODO rework, pretty sure chunk / reduceChunk does the same thing
	sum(trades = null) {

		// @TODO 
		console.log('NOTE: helper.sum is currently DISABLED');
		return;

		if( LOGGING )
			this._log.push(`helper.sum(${trades ? trades.length : 'false'})`);

		if( ! trades )
			trades = this.activeTrades;

		const initialCarry = {
			buy: { price: 0, amount: 0, trades: 0 },
			sell: { price: 0, amount: 0, trades: 0}
		};

		const sum = (
			trades
			.reduce((res, trade) => {
				const { price, amount } = trade;

				if( amount > 0 ) {
					res.buy.trades++;
					res.buy.amount += amount;
					res.buy.price += price;
				} else {
					res.sell.trades++;
					res.sell.amount += amount;
					res.sell.price += price;
				}

				return res;
			}, initialCarry )
		);

		sum.buy.price = sum.buy.price / sum.buy.trades;
		sum.sell.price = sum.sell.price / sum.sell.trades;

		return sum;
	}

	// ** Filtering ** 
	forRange(startDate, endDate, format = "D/M/YYYY") { 
		if( LOGGING )
			this._log.push(`helper.forRange(${startDate}, ${endDate}, ${format})`);

		this._updateActiveTrades( this._getRange(startDate, endDate, format) );
		
		return this;
	}
	_getRange(startDate, endDate, format = "D/M/YYYY", trades = null) {
		if( LOGGING )
			this._log.push(`helper._getRange(${startDate}, ${endDate}, ${format}, ${trades ? trades.length : 'false'})`);


		if( typeof startDate === "string" )
			startDate = this._dateToTimestamp(startDate, format);
		if( typeof endDate === "string" )
			endDate = this._dateToTimestamp(endDate, format);

		if( ! trades )
			trades = this.activeTrades;

		return trades.filter(trade => trade.timestamp >= startDate && trade.timestamp <= endDate);
	}

	// ** Internal helpers **
	_dateToTimestamp(dateStr, format = "D/M/YYYY") {
		if( LOGGING === 2 )
			this._log.push(`helper._dateToTimestamp(${dateStr}, ${format})`);

		let timestamp = moment(dateStr, format).valueOf();
		const result = parseInt( ("" + timestamp).substr(0, TIMESTAMP_LENGTH) );

		if( LOGGING === 2 )
			this._log.push(`	${result}`);

		return result;
	}
	_timestampToDate(timestamp, format = "D/M/YYYY H:m:s") {
		if( LOGGING === 2 )
			this._log.push(`helper._timestampToDate(${timestamp}, ${format})`);

		return moment.unix(timestamp).format(format);
	}
	_updateActiveTrades(trades) {
		if( LOGGING === 2 )
			this._log.push(`helper._updateActiveTrades(${trades.length})`);

		this.activeTrades = trades;
	}
	__dumpLog() {
		if( ! LOGGING )
			return;

		console.log('');
		console.log('- - - - - - - - - - - - - - - - - - - -');
		console.log('              DEBUG LOG');
		console.log('- - - - - - - - - - - - - - - - - - - -');

		this._log.forEach((log, i) => {
			console.log(`${i} : ${log.replace('helper.', '')}`); // strip out 'helper.' from log entries
		});

		console.log('- - - - - - - - - - - - - - - - - - - -');
	}
}
module.exports = Helper;

// EVERYTHING BELOW THIS SHOULD BE 
// COMMENTED OUT UNLESS DEBUGGING

// - - - - - - - - - - - - - - -
// @Debug

/*
@TODO this is wrong, negatives are to do with price action
we should treat all amounts as positive
*/
const trades = require('./trades.json');
const buyTrades  = trades.filter(trade => trade.amount > 0);
const sellTrades = trades.filter(trade => trade.amount < 0);

const helper = new Helper(buyTrades);

helper
.forRange("01/09/2017 12:0:0", "17/09/2017 12:59:59", "D/M/YYYY H:m:s")
.chunk(null, true);

// helper.__dumpLog();

helper.movingAverages();


/*
Chunked Results (buyTrades):
	chunked._2017._9._17._12._14
		{"amount":0,"trades":0,"value":0}
	chunked._2017._9._17._12
		{"amount":174263.8264184,"trades":350,"value":168.51526000000007}
	chunked._2017._9._17
		{"amount":10327682.830193019,"trades":10320,"value":4901.216460000001}
	chunked._2017._9
		{"amount":16349531.984308118,"trades":17497,"value":8388.03972}
	chunked._2017
		{"amount":16349531.984308118,"trades":17497,"value":8388.03972}
*/

/*
  Chunk Expected:
  [
	{
		meta: { type: 'chunk', 'interval': interval },
		stats: { amount, price, lowest, highest, ... },
	},
	...
  ]
*/
